package ctakesparser;

import com.fasterxml.jackson.databind.ObjectMapper;
import ctakesparser.datastructure.DiseaseDisorder;
import ctakesparser.datastructure.Medication;
import ctakesparser.datastructure.Procedure;
import ctakesparser.datastructure.Sentence;
import ctakesparser.datastructure.SignSymptom;
import ctakesparser.datastructure.UMLSConcept;
import ctakesparser.datastructure.WordToken;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Balaji
 */
public class CtakesParser {
    
    public static final String TYPE = "type";
    public static final String BEGIN = "begin";
    public static final String END = "end";
    public static final String TEXT = "text";
    public static final String ONTOLOGY = "ontology";
    public static final String CODE = "code";
    public static final String PREFERRED_TEXT = "PreferredText";

    Map<String, List<UMLSConcept>> FSArrayMap;
    Map<String, UMLSConcept> umlsConceptMap;
    Map<String, DiseaseDisorder> diseaseDisorderMap;
    Map<String, Medication> medicationMap;
    Map<String, Sentence> sentenceMap;
    Map<String, SignSymptom> signSymptomMap;
    Map<String, Procedure> procedureMap;
    Map<String, WordToken> wordTokenMap;
    
    public CtakesParser() {
        this.FSArrayMap = new TreeMap<String, List<UMLSConcept>>();
        this.umlsConceptMap = new TreeMap<String, UMLSConcept>();
        this.diseaseDisorderMap = new TreeMap<String, DiseaseDisorder>();
        this.medicationMap = new TreeMap<String, Medication>();
        this.sentenceMap = new TreeMap<String, Sentence>();
        this.signSymptomMap = new TreeMap<String, SignSymptom>();
        this.wordTokenMap = new TreeMap<String, WordToken>();
        this.procedureMap = new TreeMap<String, Procedure>();
    }
    
    public void clear() {
        this.umlsConceptMap.clear();
        this.diseaseDisorderMap.clear();
        this.medicationMap.clear();
        this.sentenceMap.clear();
        this.signSymptomMap.clear();
        this.wordTokenMap.clear();
        this.procedureMap.clear();
    }

    public Map<String, List<UMLSConcept>> getFSArrayMap() {
        return FSArrayMap;
    }

    public void setFSArrayMap(Map<String, List<UMLSConcept>> FSArrayMap) {
        this.FSArrayMap = FSArrayMap;
    }

    public Map<String, UMLSConcept> getUmlsConceptMap() {
        return umlsConceptMap;
    }

    public void setUmlsConceptMap(Map<String, UMLSConcept> umlsConceptMap) {
        this.umlsConceptMap = umlsConceptMap;
    }

    public Map<String, DiseaseDisorder> getDiseaseDisorderMap() {
        return diseaseDisorderMap;
    }

    public void setDiseaseDisorderMap(Map<String, DiseaseDisorder> diseaseDisorderMap) {
        this.diseaseDisorderMap = diseaseDisorderMap;
    }

    public Map<String, Medication> getMedicationMap() {
        return medicationMap;
    }

    public void setMedicationMap(Map<String, Medication> medicationMap) {
        this.medicationMap = medicationMap;
    }

    public Map<String, Sentence> getSentenceMap() {
        return sentenceMap;
    }

    public void setSentenceMap(Map<String, Sentence> sentenceMap) {
        this.sentenceMap = sentenceMap;
    }

    public Map<String, SignSymptom> getSignSymptomMap() {
        return signSymptomMap;
    }

    public void setSignSymptomMap(Map<String, SignSymptom> signSymptomMap) {
        this.signSymptomMap = signSymptomMap;
    }

    public Map<String, WordToken> getWordTokenMap() {
        return wordTokenMap;
    }

    public void setWordTokenMap(Map<String, WordToken> wordTokenMap) {
        this.wordTokenMap = wordTokenMap;
    }

    public Map<String, Procedure> getProcedureMap() {
        return procedureMap;
    }

    public void setProcedureMap(Map<String, Procedure> procedureMap) {
        this.procedureMap = procedureMap;
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CtakesParser parser = new CtakesParser();
//        String inputFile = "/Users/Balaji/Downloads/attachments/537772-13120800.txt.xml";
//        parser.parse(inputFile);
//        parser.writeFile("/Users/Balaji/Desktop/test.json");
        if(args.length != 2) {
            System.out.println("java -jar CtakesParser.jar <inputFolderPath> <outputFolderPath>");
            System.out.println("inputFolderPath : Absolute path to the folder containing ctakes XML output");
            System.out.println("outputFolderPath : Absolute path to the folder where the json should be written");
            System.exit(1);
        }
        File inputFolder = new File(args[0]);
        if(!inputFolder.isDirectory()) {
            System.out.println("The input is not a folder");
            System.exit(1);
        }
        File outputFolder = new File(args[1]);
        if(!outputFolder.exists()) {
            if(outputFolder.mkdirs()){
                System.out.println("Output folder does not exist. Creating the output folder specified");
            }
        }
        for(File file: inputFolder.listFiles()) {
            String filename = file.getAbsolutePath();
            if(parser == null) {
                parser = new CtakesParser();
            }
            System.out.println("Processing : " + file.getName());
            parser.parse(filename);            
            String write = outputFolder + "/" + file.getName() + ".json";    
            System.out.println("Writing to : " + write);
            parser.writeFile(write);
            parser.clear();
        }
    }
    
    public void parse(String filename) {
      try {	
//         File inputFile = new File("/Users/Balaji/Downloads/attachments/537772-13120800.txt.xml");
         File inputFile = new File(filename);
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         handleDiseaseDisorder(doc);
         handleMedication(doc);
         handleSentence(doc);
         handleSignSymptom(doc);
         handleUMLSConcept(doc);
         handleWordToken(doc);
         handleProcedure(doc);
         handleFSArray(doc);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
    
    public void handleSentence(Document doc) {
       NodeList sentenceList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.textspan.Sentence");
         for (int temp = 0; temp < sentenceList.getLength(); temp++) {
            Node nNode = sentenceList.item(temp);
            Sentence sentence = new Sentence();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               sentence.setBegin(eElement.getAttribute("begin"));
               sentence.setEnd(eElement.getAttribute("end"));
               sentence.setId(eElement.getAttribute("_id"));
               sentence.setRef_sofa(eElement.getAttribute("_ref_sofa"));
               sentence.setSentenceNumber(eElement.getAttribute("sentenceNumber"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               sentenceMap.put(key, sentence);
            }
         }
   } 
    
    public void handleDiseaseDisorder(Document doc) {
       NodeList diseaseDisorderList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.textsem.DiseaseDisorderMention");
         for (int temp = 0; temp < diseaseDisorderList.getLength(); temp++) {
            Node nNode = diseaseDisorderList.item(temp);
            DiseaseDisorder dd = new DiseaseDisorder();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               dd.setBegin(eElement.getAttribute("begin"));
               dd.setEnd(eElement.getAttribute("end"));
               dd.setConditional(eElement.getAttribute("conditional"));
               dd.setConfidence(eElement.getAttribute("confidence"));
               dd.setGeneric(eElement.getAttribute("generic"));
               dd.setHistoryOf(eElement.getAttribute("historyOf"));
               dd.setId(eElement.getAttribute("_id"));
               dd.setPolarity(eElement.getAttribute("polarity"));
               dd.setRef_ontologyConceptArr(eElement.getAttribute("_ref_ontologyConceptArr"));
               dd.setRef_sofa(eElement.getAttribute("_ref_sofa"));
               dd.setSubject(eElement.getAttribute("subject"));
               dd.setUncertainty(eElement.getAttribute("uncertainty"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               diseaseDisorderMap.put(key, dd);
            }
         }
   }
    
   public void handleProcedure(Document doc) {
       NodeList diseaseDisorderList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.textsem.ProcedureMention");
         for (int temp = 0; temp < diseaseDisorderList.getLength(); temp++) {
            Node nNode = diseaseDisorderList.item(temp);
            DiseaseDisorder dd = new DiseaseDisorder();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               dd.setBegin(eElement.getAttribute("begin"));
               dd.setEnd(eElement.getAttribute("end"));
               dd.setConditional(eElement.getAttribute("conditional"));
               dd.setConfidence(eElement.getAttribute("confidence"));
               dd.setGeneric(eElement.getAttribute("generic"));
               dd.setHistoryOf(eElement.getAttribute("historyOf"));
               dd.setId(eElement.getAttribute("_id"));
               dd.setPolarity(eElement.getAttribute("polarity"));
               dd.setRef_ontologyConceptArr(eElement.getAttribute("_ref_ontologyConceptArr"));
               dd.setRef_sofa(eElement.getAttribute("_ref_sofa"));
               dd.setSubject(eElement.getAttribute("subject"));
               dd.setUncertainty(eElement.getAttribute("uncertainty"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               diseaseDisorderMap.put(key, dd);
            }
         }
   }
    
   public void handleFSArray(Document doc) {
       NodeList fsArrayList = doc.getElementsByTagName("uima.cas.FSArray");
         for (int temp = 0; temp < fsArrayList.getLength(); temp++) {
            Node nNode = fsArrayList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               String key =  eElement.getAttribute("_id");
               NodeList elementList = eElement.getElementsByTagName("i");
               List<UMLSConcept> umlsList = new ArrayList<UMLSConcept>();
               for(int i = 0 ; i < elementList.getLength(); i++) {                  
                   Node node = elementList.item(i);
                   Element element = (Element) node;
                   umlsList.add(umlsConceptMap.get(element.getTextContent()));
               }
               FSArrayMap.put(key, umlsList);
            }
         }
   }
   
   
   public void handleUMLSConcept(Document doc) {
       NodeList umlsConceptList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.refsem.UmlsConcept");
         for (int temp = 0; temp < umlsConceptList.getLength(); temp++) {
            Node nNode = umlsConceptList.item(temp);
            UMLSConcept concept = new UMLSConcept();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               concept.setCode(eElement.getAttribute("code"));
               concept.setCodingScheme(eElement.getAttribute("codingScheme"));
               concept.setId(eElement.getAttribute("_id"));
               concept.setCui(eElement.getAttribute("cui"));
               concept.setOid(eElement.getAttribute("oid"));
               concept.setDisambiguated(Boolean.parseBoolean(eElement.getAttribute("disambiguated")));
               concept.setPreferredText(eElement.getAttribute("preferredText"));
               concept.setScore(eElement.getAttribute("score"));
               concept.setTui(eElement.getAttribute("tui"));
               umlsConceptMap.put(eElement.getAttribute("_id"), concept);
            }
         }
   }
   
      public void handleWordToken(Document doc) {
       NodeList wordTokenList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.syntax.WordToken");
         for (int temp = 0; temp < wordTokenList.getLength(); temp++) {
            Node nNode = wordTokenList.item(temp);
            WordToken wordToken = new WordToken();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               wordToken.setBegin(eElement.getAttribute("begin"));
               wordToken.setEnd(eElement.getAttribute("end"));
               wordToken.setId(eElement.getAttribute("_id"));
               wordToken.setCapitalization(eElement.getAttribute("capitalization"));
               wordToken.setNormalizedForm(eElement.getAttribute("normalizedForm"));
               wordToken.setNumPosition(eElement.getAttribute("numPosition"));
               wordToken.setPartOfSpeech(eElement.getAttribute("partOfSpeech"));
               wordToken.setTokenNumber(eElement.getAttribute("tokenNumber"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               wordTokenMap.put(key, wordToken);
            }
         }
   }
    
    
   public void handleSignSymptom(Document doc) {
       NodeList SignSymptomList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.textsem.SignSymptomMention");
         for (int temp = 0; temp < SignSymptomList.getLength(); temp++) {
            Node nNode = SignSymptomList.item(temp);
            SignSymptom sign = new SignSymptom();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               sign.setBegin(eElement.getAttribute("begin"));
               sign.setEnd(eElement.getAttribute("end"));
               sign.setConditional(eElement.getAttribute("conditional"));
               sign.setConfidence(eElement.getAttribute("confidence"));
               sign.setGeneric(eElement.getAttribute("generic"));
               sign.setHistoryOf(eElement.getAttribute("historyOf"));
               sign.setId(eElement.getAttribute("_id"));
               sign.setPolarity(eElement.getAttribute("polarity"));
               sign.setRef_ontologyConceptArr(eElement.getAttribute("_ref_ontologyConceptArr"));
               sign.setRef_sofa(eElement.getAttribute("_ref_sofa"));
               sign.setSubject(eElement.getAttribute("subject"));
               sign.setUncertainty(eElement.getAttribute("uncertainty"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               signSymptomMap.put(key, sign);
            }
            
         }
    }
   
   
    public void handleMedication(Document doc) {
       NodeList medicationList = doc.getElementsByTagName("org.apache.ctakes.typesystem.type.textsem.MedicationMention");
         for (int temp = 0; temp < medicationList.getLength(); temp++) {
            Node nNode = medicationList.item(temp);
            Medication medication = new Medication();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               medication.setBegin(eElement.getAttribute("begin"));
               medication.setEnd(eElement.getAttribute("end"));
               medication.setConditional(eElement.getAttribute("conditional"));
               medication.setConfidence(eElement.getAttribute("confidence"));
               medication.setGeneric(eElement.getAttribute("generic"));
               medication.setHistoryOf(eElement.getAttribute("historyOf"));
               medication.setId(eElement.getAttribute("_id"));
               medication.setPolarity(eElement.getAttribute("polarity"));
               medication.setRef_ontologyConceptArr(eElement.getAttribute("_ref_ontologyConceptArr"));
               medication.setRef_sofa(eElement.getAttribute("_ref_sofa"));
               medication.setSubject(eElement.getAttribute("subject"));
               medication.setUncertainty(eElement.getAttribute("uncertainty"));
               medication.setRef_medicationStatusChange(eElement.getAttribute("_ref_medicationStatusChange"));
               String key = eElement.getAttribute("begin") + "_" + eElement.getAttribute("end");
               medicationMap.put(key, medication);
            }
            
         }
   }
      
      
    public void writeFile(String filename) {
        List<JSONObject> jsonObj = new ArrayList<JSONObject>();    
        for(Map.Entry<String, Medication> entry : medicationMap.entrySet()) {
            JSONObject medication = new JSONObject();
            medication.put(TYPE, "MedicationMention");
            medication.put(BEGIN, entry.getValue().getBegin());
            medication.put(END, entry.getValue().getEnd());
            List<String> concepts = new ArrayList<String>();
            List<String> preferredTexts = new ArrayList<String>();
            if(entry.getValue().getRef_ontologyConceptArr().trim().length() > 0) {
                for(int i = 0; i < FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).size(); i++) {
                    concepts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getCode());
                    preferredTexts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getPreferredText());
                }
                UMLSConcept concept = FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(0);
                medication.put(ONTOLOGY, concept.getCodingScheme());
                medication.put(CODE, concepts);
                medication.put(TEXT, getWord(Integer.parseInt(entry.getValue().getBegin()), Integer.parseInt(entry.getValue().getEnd())));
                medication.put(PREFERRED_TEXT,preferredTexts);
                    jsonObj.add(medication);
            }
        }
        
        for(Map.Entry<String, DiseaseDisorder> entry : diseaseDisorderMap.entrySet()) {
            JSONObject diseaseDisorder = new JSONObject();
            diseaseDisorder.put(TYPE, "DiseaseDisorderMention");
            diseaseDisorder.put(BEGIN, entry.getValue().getBegin());
            diseaseDisorder.put(END, entry.getValue().getEnd());
            List<String> concepts = new ArrayList<String>();
            List<String> preferredTexts = new ArrayList<String>();
            if(entry.getValue().getRef_ontologyConceptArr().trim().length() > 0) {
                for(int i = 0; i < FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).size(); i++) {
                    concepts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getCode());
                    preferredTexts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getPreferredText());
                }
                UMLSConcept concept = FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(0);
                diseaseDisorder.put(ONTOLOGY, concept.getCodingScheme());
                diseaseDisorder.put(CODE, concepts);
                diseaseDisorder.put(TEXT, getWord(Integer.parseInt(entry.getValue().getBegin()), Integer.parseInt(entry.getValue().getEnd()))); 
                diseaseDisorder.put(PREFERRED_TEXT, preferredTexts);
                jsonObj.add(diseaseDisorder);
            }
        }
        
        for(Map.Entry<String, SignSymptom> entry : signSymptomMap.entrySet()) {
            JSONObject signSymptom = new JSONObject();
            signSymptom.put(TYPE, "SignSymptomMention");
            signSymptom.put(BEGIN, entry.getValue().getBegin());
            signSymptom.put(END, entry.getValue().getEnd());
            List<String> concepts = new ArrayList<String>();
            List<String> preferredTexts = new ArrayList<String>();
            if(entry.getValue().getRef_ontologyConceptArr().trim().length() > 0) {
                for(int i = 0; i < FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).size(); i++) {
                    concepts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getCode());
                    preferredTexts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getPreferredText());
                }
                UMLSConcept concept = FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(0);
                signSymptom.put(ONTOLOGY, concept.getCodingScheme());
                signSymptom.put(CODE, concepts);
                signSymptom.put(TEXT, getWord(Integer.parseInt(entry.getValue().getBegin()), Integer.parseInt(entry.getValue().getEnd()))); 
                signSymptom.put(PREFERRED_TEXT, preferredTexts);
                jsonObj.add(signSymptom);
            }
        }
        
        for(Map.Entry<String, Procedure> entry : procedureMap.entrySet()) {
            JSONObject procedure = new JSONObject();
            procedure.put(TYPE, "ProcedureMention");
            procedure.put(BEGIN, entry.getValue().getBegin());
            procedure.put(END, entry.getValue().getEnd());
            List<String> concepts = new ArrayList<String>();
            List<String> preferredTexts = new ArrayList<String>();
            if(entry.getValue().getRef_ontologyConceptArr().trim().length() > 0) {
                for(int i = 0; i < FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).size(); i++) {
                    concepts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getCode());
                    preferredTexts.add(FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(i).getPreferredText());
                }
                UMLSConcept concept = FSArrayMap.get(entry.getValue().getRef_ontologyConceptArr()).get(0);
                procedure.put(ONTOLOGY, concept.getCodingScheme());
                procedure.put(CODE, concepts);
                procedure.put(TEXT, getWord(Integer.parseInt(entry.getValue().getBegin()), Integer.parseInt(entry.getValue().getEnd()))); 
                procedure.put(PREFERRED_TEXT, preferredTexts);
                jsonObj.add(procedure);
            }
        }
        
        try (FileWriter file = new FileWriter(filename)) {
            ObjectMapper mapper = new ObjectMapper();
            file.write(mapper.writeValueAsString(jsonObj));
        } catch (IOException ex) {
            Logger.getLogger(CtakesParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
   
    
    public String getWord(int begin, int end) {
        String returnStr = "";
        for(Map.Entry<String, WordToken> entry: wordTokenMap.entrySet()) {
            WordToken wt = entry.getValue();
            if(Integer.parseInt(wt.getBegin()) >= begin && Integer.parseInt(wt.getEnd()) <= end) {
                returnStr += wt.getNormalizedForm() + " ";
            }
        }
        return returnStr.trim();
    }
}

