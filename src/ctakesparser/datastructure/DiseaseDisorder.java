/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ctakesparser.datastructure;

/**
 *
 * @author Balaji
 */
public class DiseaseDisorder {
    String id;
    String _ref_sofa;
    String begin;
    String end;
    String _ref_ontologyConceptArr;
    String confidence;
    String polarity;
    String uncertainty;
    String conditional;
    String generic;
    String subject;
    String historyOf;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRef_sofa() {
        return _ref_sofa;
    }

    public void setRef_sofa(String _ref_sofa) {
        this._ref_sofa = _ref_sofa;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getRef_ontologyConceptArr() {
        return _ref_ontologyConceptArr;
    }

    public void setRef_ontologyConceptArr(String _ref_ontologyConceptArr) {
        this._ref_ontologyConceptArr = _ref_ontologyConceptArr;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public String getPolarity() {
        return polarity;
    }

    public void setPolarity(String polarity) {
        this.polarity = polarity;
    }

    public String getUncertainty() {
        return uncertainty;
    }

    public void setUncertainty(String uncertainty) {
        this.uncertainty = uncertainty;
    }

    public String getConditional() {
        return conditional;
    }

    public void setConditional(String conditional) {
        this.conditional = conditional;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHistoryOf() {
        return historyOf;
    }

    public void setHistoryOf(String historyOf) {
        this.historyOf = historyOf;
    }
    
    
}
