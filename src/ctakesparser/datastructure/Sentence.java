/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ctakesparser.datastructure;

/**
 *
 * @author Balaji
 */
public class Sentence {
    String id;
    String _ref_sofa;
    String begin;
    String end;
    String sentenceNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRef_sofa() {
        return _ref_sofa;
    }

    public void setRef_sofa(String _ref_sofa) {
        this._ref_sofa = _ref_sofa;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getSentenceNumber() {
        return sentenceNumber;
    }

    public void setSentenceNumber(String sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }
    
    
}
