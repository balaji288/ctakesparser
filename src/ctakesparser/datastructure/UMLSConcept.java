/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ctakesparser.datastructure;

/**
 *
 * @author Balaji
 */
public class UMLSConcept {
    String id; 
    String codingScheme;
    String code;
    String oid;
    String score;
    boolean disambiguated;
    String cui;
    String tui;
    String preferredText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodingScheme() {
        return codingScheme;
    }

    public void setCodingScheme(String codingScheme) {
        this.codingScheme = codingScheme;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public boolean isDisambiguated() {
        return disambiguated;
    }

    public void setDisambiguated(boolean disambiguated) {
        this.disambiguated = disambiguated;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getTui() {
        return tui;
    }

    public void setTui(String tui) {
        this.tui = tui;
    }

    public String getPreferredText() {
        return preferredText;
    }

    public void setPreferredText(String preferredText) {
        this.preferredText = preferredText;
    }
    
    
}
