/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ctakesparser.datastructure;

/**
 *
 * @author Balaji
 */
public class WordToken {
    String id;
    String begin;
    String end;
    String tokenNumber;
    String normalizedForm;
    String partOfSpeech;
    String capitalization;
    String numPosition;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public String getNormalizedForm() {
        return normalizedForm;
    }

    public void setNormalizedForm(String normalizedForm) {
        this.normalizedForm = normalizedForm;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getCapitalization() {
        return capitalization;
    }

    public void setCapitalization(String capitalization) {
        this.capitalization = capitalization;
    }

    public String getNumPosition() {
        return numPosition;
    }

    public void setNumPosition(String numPosition) {
        this.numPosition = numPosition;
    }

    
    
}
